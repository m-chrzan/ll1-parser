Object subclass: Grammar [
    | start productions nonterminals generates nulls reachable first follow
      select |

    hash := Object new.
    S := Object new.

    Grammar class >> hash [
        ^hash.
    ]

    nonterminals [
        ^nonterminals.
    ]

    start [
        ^start.
    ]

    productions [
        ^productions.
    ]

    nonterminals [
        ^nonterminals.
    ]

    generates: n [
        ^generates at: n.
    ]

    nulls: n [
        ^nulls at: n.
    ]

    first: n [
        ^first at: n.
    ]

    reachable: n [
        ^reachable at: n.
    ]

    follow: n [
        ^follow at: n.
    ]

    select: rule [
        ^(select at: rule key) at: rule value
    ]

    selectFor: n and: a [
        (select at: n) keysAndValuesDo: [ :production :letters |
            (letters includes: a) ifTrue: [
                ^production.
            ].
        ].

        ^nil.
    ]

    Grammar class >> start: s rules: r [
        | g |
        g := Grammar new.
        g start: s rules: r.
        ^g.
    ]

    start: s rules: rules [
        start := s.

        nonterminals := (Set with: start with: S),
                        (rules asSet collect: [ :rule | rule key ]).

        productions := Dictionary new.
        nonterminals do: [ :n | productions at: n put: Set new. ].
        rules do: [ :rule |
            (productions at: rule key) add: rule value.
        ].

        productions at: S put:
            (Set with: (OrderedCollection with: start with: hash)).

        self initializeSets.
    ]

    initializeSets [
        self initializeGenerates.
        self initializeNulls.
        self initializeReachable.
        self initializeFirst.
        self initializeFollow.
        self initializeSelect.
    ]

    initializeGenerates [
        generates := Dictionary new.
        nonterminals do: [ :n | generates at: n put: false ].
        [ self initializeGeneratesOneIteration. ] whileTrue: []
    ]

    initializeGeneratesOneIteration [
        ^nonterminals inject: false into: [ :foundNew :n |
            | newFoundNew |
            newFoundNew := foundNew.
            (generates at: n) ifFalse: [
                (self checkIfNonterminalGenerates: n) ifTrue: [
                    generates at: n put: true.
                    newFoundNew := true.
                ].
            ].

            newFoundNew.
        ].
    ]

    checkIfNonterminalGenerates: n [
        ^(productions at: n) anySatisfy: [ :production |
            self checkIfProductionGenerates: production.
        ].
    ]

    checkIfProductionGenerates: production [
        ^production noneSatisfy: [ :a |
            (nonterminals includes: a) and: [ (generates at: a) not ].
        ].
    ]

    initializeNulls [
        nulls := Dictionary new.
        nonterminals do: [ :n | nulls at: n put: false ].
        [ self initializeNullsOneIteration. ] whileTrue: []
    ]

    initializeNullsOneIteration [
        ^nonterminals inject: false into: [ :foundNew :n |
            | newFoundNew |
            newFoundNew := foundNew.
            (nulls at: n) ifFalse: [
                (self checkIfNonterminalNulls: n) ifTrue: [
                    nulls at: n put: true.
                    newFoundNew := true.
                ].
            ].

            newFoundNew.
        ].
    ]

    checkIfNonterminalNulls: n [
        ^(productions at: n) anySatisfy: [ :production |
            self checkIfProductionNulls: production.
        ].
    ]

    checkIfProductionNulls: production [
        ^production allSatisfy: [ :a |
            (nonterminals includes: a) and: [ (nulls at: a) ].
        ].
    ]

    initializeReachable [
        | toVisit visited |
        reachable := Dictionary new.
        nonterminals do: [ :n | reachable at: n put: false ].
        reachable at: start put: true.
        toVisit := Set with: start.
        visited := Set with: start.

        [ toVisit notEmpty ] whileTrue: [
            | current |
            current := toVisit anyOne.
            toVisit remove: current ifAbsent: [].

            (productions at: current) do: [ :production |
                (production allSatisfy: [ :a |
                    ((nonterminals includes: a) not) or: [ generates at: a ].
                ]) ifTrue: [
                    production do: [ :a |
                        ((nonterminals includes: a) and:
                            [ (visited includes: a) not ]) ifTrue: [
                            visited add: a.
                            toVisit add: a.
                            reachable at: a put: true.
                        ].
                    ].
                ].
            ].
        ].
    ]

    initializeFirst [
        first := Dictionary new.
        nonterminals do: [ :n | first at: n put: Set new ].
        [ self initializeFirstOneIteration. ] whileTrue: [].
    ]

    initializeFirstOneIteration [
        ^nonterminals inject: false into: [ :foundNew :n |
            | newFoundNew firsts |
            newFoundNew := foundNew.
            firsts := first at: n.

            (productions at: n) do: [ :production |
                | newFirsts |
                newFirsts := self gatherFirsts: production.

                (newFirsts - firsts) notEmpty ifTrue: [
                    newFoundNew := true.
                ].

                firsts := firsts + newFirsts.
            ].

            first at: n put: firsts.

            newFoundNew.
        ].
    ]

    gatherFirsts: word [
        | firsts |
        firsts := Set new.
        word do: [ :a |
            ((nonterminals includes: a) and: [ (nulls at: a) ]) ifTrue: [
                firsts := firsts + ((first at: a) copyWithout: hash).
            ] ifFalse: [
                (nonterminals includes: a) ifTrue: [
                    firsts := firsts + (first at: a).
                ] ifFalse: [
                    firsts add: a.
                ].
                ^firsts.
            ].
        ].

        firsts add: hash.

        ^firsts.
    ]

    initializeFollow [
        follow := Dictionary new.
        nonterminals do: [ :n | follow at: n put: (self findFirstFollow: n) ].
        [ self initializeFollowOneIteration ] whileTrue: [].
    ]

    findFirstFollow: n [
        | foll |
        foll := Set new.
        nonterminals do: [ :nonterminal |
            (reachable at: nonterminal) ifTrue: [
                productions do: [ :prods |
                    prods do: [ :production |
                        foll := foll +
                            ((self followBasedOn: production for: n)
                            copyWithout: hash).
                    ].
                ].
            ].
        ].

        ^foll.
    ]

    followBasedOn: production for: n [
        | foll |
        foll := Set new.
        (Interval from: 1 to: production size) do: [ :i |
            (production at: i) = n ifTrue: [
                | suffix |
                suffix := production allButFirst: i.
                foll := foll + (self gatherFirsts: suffix).
            ].
        ].

        ^foll.
    ]

    initializeFollowOneIteration [
        ^nonterminals inject: false into: [ :foundNew :n |
            | newFoundNew |
            newFoundNew := foundNew.
            (productions at: n) do: [ :production |
                (Interval from: 1 to: production size) do: [ :i |
                    | a |
                    a := production at: i.
                    ((nonterminals includes: a) and: [
                        (production allButFirst: i) allSatisfy: [ :b |
                            (nonterminals includes: b) and: [ nulls at: b ]
                        ].
                    ]) ifTrue: [
                        | oldFollow newFollow |
                        oldFollow := follow at: a.
                        newFollow := follow at: n.
                        ((newFollow - oldFollow) notEmpty) ifTrue: [
                            follow at: a put: (oldFollow + newFollow).
                            newFoundNew := true.
                        ].
                    ].
                ].
            ].

            newFoundNew.
        ].
    ]

    initializeSelect [
        select := Dictionary new.
        nonterminals do: [ :n |
            | sel |
            sel := select at: n put: Dictionary new.
            (productions at: n) do: [ :production |
                (production allSatisfy: [ :a |
                    (nonterminals includes: a) and: [ nulls at: a ].
                ]) ifTrue: [
                    sel at: production put:
                        ((self gatherFirsts: production) copyWithout: hash) +
                        (follow at: n).
                ] ifFalse: [
                    sel at: production put: (self gatherFirsts: production).
                ].
            ].
        ].
    ]

    isLL1 [
        select do: [ :sel |
            | seenSoFar |
            seenSoFar := Set new.
            sel do: [ :symbols |
                ((seenSoFar & symbols) isEmpty) ifTrue: [
                    seenSoFar := seenSoFar + symbols.
                ] ifFalse: [
                    ^false.
                ].
            ].
        ].

        ^true.
    ]
].
