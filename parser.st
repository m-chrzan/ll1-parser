Object subclass: ParserLL1 [
    | grammar stack isValid |

    grammar: g [
        grammar := g.
    ]

    stack: s [
        stack := s.
    ]

    isValid: valid [
        isValid := valid.
    ]

    isValid [
        ^isValid.
    ]

    setup: g [
        grammar := g.
        stack := OrderedCollection with: (grammar start).
        isValid := (grammar generates: grammar start).
    ]

    ParserLL1 class >> start: s rules: r [
        | g p |
        g := Grammar start: s rules: r.
        g isLL1 ifFalse: [ ^nil ].
        p := ParserLL1 new.
        p setup: g.
        ^p.
    ]

    reject [
        ^isValid not.
    ]

    accept [
        ^isValid and: [
            stack allSatisfy: [ :a |
                (grammar nonterminals includes: a) and: [ grammar nulls: a ].
            ].
        ].
    ]

    predict [
        ^(grammar gatherFirsts: stack reverse) copyWithout: Grammar hash.
    ]

    react: a [
        | p |
        p := ParserLL1 new.
        p isValid: true.
        p grammar: grammar.
        p stack: stack.
        p readLetter: a.
        ^p
    ]

    readLetter: a [
        [ grammar nonterminals includes: stack last. ] whileTrue: [
            | production |
            production := grammar selectFor: stack last and: a.
            (self productionValid: production) ifFalse: [
                isValid := false.
                ^nil.
            ].
            stack := stack allButLast, production reverse.
        ].

        (stack last = a) ifTrue: [
            stack := stack allButLast.
        ] ifFalse: [
            isValid := false.
        ].
    ]

    productionValid: production [
        production isNil ifTrue: [ ^false ].
        ^production allSatisfy: [ :a |
            ((grammar nonterminals includes: a) not) or: [
                grammar generates: a
            ]
        ].
    ]
]
